from orbit_component_base.src.orbit_plugin import PluginBase, ArgsBase
from orbit_component_buttonlogout.schema.MyTable import MyTableCollection
from loguru import logger as log


class Plugin (PluginBase):

    NAMESPACE = 'buttonlogout'
    COLLECTIONS = [MyTableCollection]


class Args (ArgsBase):
        
    def setup (self):
        self._parser.add_argument("--dummy", action='store_true', help="Add your CLI options here")
        return self
    
    def process (self):
        if self._args.dummy:
            self._parser.error('Insert your custom processing code here')
            exit()