var buttonMod
if (import.meta.env.DEV) {
    buttonMod = await import('@/../node_modules/orbit-component-buttonlogout/index.js');
} 
else {
    buttonMod = await import('@/../node_modules/orbit-component-buttonlogout')
}
const buttonlogout = buttonMod.default

import LogOutOutlined from '@vicons/material/LogOutOutlined.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconLogout = shallowRef(LogOutOutlined)
    app.use(buttonlogout, {
        router: router,
        menu: menu,
        root: 'buttonlogout',
        buttons: [
            {name: 'buttonlogout', text: 'Logout'  , component: buttonlogout , icon: IconLogout , pri: 99, path: '/buttonlogout', meta: {root: 'buttonlogout', host: location.host}},
        ]
    })
}