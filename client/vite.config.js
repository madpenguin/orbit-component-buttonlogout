import { fileURLToPath, URL } from 'node:url'
const path = require('path');
const { defineConfig } = require('vite');
const vue = require('@vitejs/plugin-vue');
const component = 'orbit-component-buttonlogout'
import cssInjectedByJsPlugin from 'vite-plugin-css-injected-by-js'
import pkg from './package.json';

export default defineConfig({
  build: {
    lib: {
      entry: path.resolve(__dirname, 'index.js'),
      name: component,
      fileName: (format) => `${component}.${format}.js`,
    },
    rollupOptions: {
      external: Object.keys(pkg.dependencies || {}),
      output: [{
          format: "es",
          exports: "default",
          globals: {vue: 'Vue'}
      }],
    },
    commonjsOptions: {
      esmExternals: true 
    },
  },
  plugins: [vue(), cssInjectedByJsPlugin()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }  
});